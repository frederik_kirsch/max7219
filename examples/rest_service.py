#!flask/bin/python

from threading import Thread

import max7219.led as led
from max7219.font import proportional, SINCLAIR_FONT, TINY_FONT, CP437_FONT

from flask import Flask, jsonify, request, abort

app = Flask(__name__)

print("Service started")
content = "Cherriz HA"
# Matrix erzeugen (5 in Reihe)
device = led.matrix(cascaded=5)
print("Device created")


@app.route('/cherriz/ha/hw/display', methods=['GET'])
def get_content():
    print("Request content " + content)
    return jsonify({'content': content})


@app.route('/cherriz/ha/hw/display', methods=['POST'])
def set_content():
    global content
    if not request.json or 'content' not in request.json:
        print("Illegal Request" + request)
        abort(400)
    content = request.json['content']
    start_content_thread(content)
    return jsonify({'status': 'success'}), 200


def start_content_thread(text):
    # Darstellung in eigenem Thread damit Prozess durch HW nicht blockiert wird.
    thread = Thread(target=display_text, args=(text,))
    thread.start()


def display_text(text):
    print("Set content " + content)
    device.show_message(text, font=proportional(TINY_FONT))


if __name__ == '__main__':
    start_content_thread(content)
    # 0.0.0.0 sorgt dafuer das der Service auch von aussen erreichbar ist.
    app.run(host='0.0.0.0')
